using UnityEngine;

public class PathProjectionDebug : MonoBehaviour
{
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;
    [SerializeField] private Transform vehicle;
    [SerializeField] private Transform lookAhead;
    [SerializeField] private float offset = 0.5f;
    private LineSegment lineSegment;

    private void Start()
    {
        lineSegment = new LineSegment(start.position, end.position, 1f);
    }

    private void Update()
    {
        UpdateLineSegment();
        Project();
    }

    private void UpdateLineSegment()
    {
        lineSegment.Start = start.position;
        lineSegment.End = end.position;
    }

    private void Project()
    {
        //TODO: Wenn er super weit weg ist (zweiter Path-Radius zum testen) target auf n�chsten lineSegment.start setzen
        Vector3 start = lineSegment.Start;
        Vector3 end = lineSegment.End;
        float radius = lineSegment.Radius;
        Vector3 futurePos = lookAhead.position;
        Debug.DrawLine(vehicle.transform.position, futurePos, Color.blue);
        Vector3 startToFuturePos = futurePos - start;
        Debug.DrawRay(start, startToFuturePos, new Color(0f, 0.7f, 0.5f));
        Vector3 startToEnd = (end - start);
        Debug.DrawRay(start, startToEnd, Color.green);
        Vector3 startToEndDir = startToEnd.normalized;
        Debug.DrawRay(start, startToEndDir, Color.white);
        float dot = Vector3.Dot(startToFuturePos, startToEndDir);
        dot = Mathf.Abs(dot);
        Vector3 projectedDir = startToEndDir * dot;
        Vector3 offsetDir = startToEndDir * offset;
        Vector3 normalPoint = start + projectedDir;
        Vector3 target = normalPoint + offsetDir;
        Debug.DrawLine(start, normalPoint, Color.cyan);
        Debug.DrawLine(normalPoint, target, Color.red);
        Debug.DrawLine(futurePos, normalPoint, new Color(1f, 0.7f, 0.25f, 1));
        Debug.DrawLine(futurePos, target, Color.yellow);
    }
}
