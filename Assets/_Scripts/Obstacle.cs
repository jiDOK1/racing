﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float Radius;
    public Vector3 Position;

    private void Awake()
    {
        Position = transform.position;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, Radius);
    }
}
