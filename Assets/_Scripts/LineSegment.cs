﻿using UnityEngine;

//[System.Serializable]
public class LineSegment
{
    public Vector3 Start;
    public Vector3 End;
    public float Radius;

    public LineSegment() { }

    public LineSegment(Vector3 start, Vector3 end, float radius)
    {
        Start = start;
        End = end;
        Radius = radius;
    }
}
