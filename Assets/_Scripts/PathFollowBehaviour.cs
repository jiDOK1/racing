using UnityEngine;

public class PathFollowBehaviour : SteeringBehaviour
{
    public TextMesh debug;
    public float predDist = 2f;
    public LineSegment[] LineSegments;
    private LineSegment curLineSegment;
    private int curLsIdx;

    public override Vector3 Steer()
    {
        Vector3 start = curLineSegment.Start;
        Vector3 end = curLineSegment.End;
        float radius = curLineSegment.Radius;
        Vector3 futurePos = transform.position + vehicle.Velocity.normalized * predDist;
        Debug.DrawLine(transform.position, futurePos, Color.blue);
        Vector3 startToFuturePos = futurePos - start;
        Debug.DrawLine(start, end, Color.green);
        Vector3 startToEnd = (end - start);
        Vector3 startToEndDir = startToEnd.normalized;
        float dot = Vector3.Dot(startToFuturePos, startToEndDir);
        dot = Mathf.Abs(dot);
        Vector3 normalPoint = start + (startToEndDir * dot);
        Debug.DrawRay(start, startToEndDir * dot , Color.black);

        debug.text = $"{(startToEndDir * dot).sqrMagnitude} \n {startToEnd.sqrMagnitude} ";
        if ((startToEndDir * dot).sqrMagnitude > startToEnd.sqrMagnitude)
        {
            curLsIdx = (curLsIdx + 1) % LineSegments.Length;
            SetData(curLsIdx);
        }
        //debug.text = curLsIdx.ToString();

        //Debug.DrawRay(transform.position, vehicle.Velocity, Color.red);
        Debug.DrawLine(futurePos, normalPoint, Color.cyan);

        if ((futurePos - normalPoint).sqrMagnitude > radius)
        {
            Vector3 desired = normalPoint - transform.position;
            desired.Normalize();
            desired *= vehicle.MaxSpeed;
            Vector3 steer = desired - vehicle.Velocity;
            steer = Vector3.ClampMagnitude(steer, vehicle.MaxForce);
            return steer;
        }
        else
        {
            return Vector3.zero;
        }
    }

    public void SetData(int lineIdx)
    {
        curLineSegment = LineSegments[lineIdx];
    }
}
