﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuloTest : MonoBehaviour
{
    public int length = 5;
    private int curIdx = 0;

    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            Debug.Log("before modulo:");
            Debug.Log(curIdx);
            curIdx = (curIdx + 1) % length;
            Debug.Log("after modulo:");
            Debug.Log(curIdx);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
