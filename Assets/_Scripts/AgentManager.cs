using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    public Obstacle[] Obstacles;
    public Vehicle[] Vehicles;
    public Transform[] Waypoints;
    public LinePath LinePath;
    bool playing;

    private void Start()
    {
        playing = true;
        foreach (Vehicle vehicle in Vehicles)
        {
            var SBs = new List<SteeringBehaviour>(vehicle.steeringBehaviours);
            var sortedSBs = SBs.OrderBy(p => p.data.Priority);
            vehicle.steeringBehaviours = sortedSBs.ToArray();

            SeparationBehaviour sb = vehicle.GetComponent<SeparationBehaviour>();
            if (sb != null)
            {
                sb.vehicles = Vehicles;
            }
            ObstacleAvoidBehaviour oab = vehicle.GetComponent<ObstacleAvoidBehaviour>();
            if (oab != null)
            {
                oab.Obstacles = Obstacles;
            }
            PathFollowBehaviour pfb = vehicle.GetComponent<PathFollowBehaviour>();
            if(pfb != null)
            {
                pfb.LineSegments = LinePath.LineSegments;
                pfb.SetData(0);
            }

            //WaypointFollow wpf = vehicle.GetComponent<WaypointFollow>();
            //if (wpf != null)
            //{
            //    wpf.Waypoints = Waypoints;
            //    wpf.CurTarget = Waypoints[0];
            //}
        }
    }

    void FixedUpdate()
    {
        for (int v = 0; v < Vehicles.Length; v++)
        {
            Vehicle vehicle = Vehicles[v];
            if (!vehicle.isActiveAndEnabled) { continue; }
            SteeringBehaviour[] steeringBehaviours = vehicle.steeringBehaviours;
            for (int s = 0; s < steeringBehaviours.Length; s++)
            {
                var sb = steeringBehaviours[s];
                Vector3 steer = sb.Steer();
                vehicle.ApplyForce(steer, sb.data.Weight, out bool forceExhausted);
                if (forceExhausted) { break; }
            }
            vehicle.UpdateVehicle();
        }
    }

    //private void OnDrawGizmos()
    //{
    //    if (!playing)
    //    {
    //        return;
    //    }
    //    for (int i = 0; i < Vehicles.Length; i++)
    //    {
    //        var v = Vehicles[i];
    //        //Handles.Label(v.transform.position, $"Oki doki");
    //        //if (!v.isActiveAndEnabled) { return; }
    //        if (v.ShowDebug)
    //        {
    //            Handles.Label(v.transform.position, GlobalDebug.Info);
    //            Gizmos.DrawWireSphere(v.transform.position, 2f);
    //        }
    //    }
    //}
}
